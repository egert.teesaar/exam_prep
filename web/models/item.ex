defmodule Auction.Item do
  use Auction.Web, :model

  schema "items" do
    field :name, :string
    field :size, :string
    field :price, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :size, :price])
    |> validate_required([:name, :size, :price])
  end
end
