defmodule Auction.Repo.Migrations.CreateItem do
  use Ecto.Migration

  def change do
    create table(:items) do
      add :name, :string
      add :size, :string
      add :price, :string

      timestamps()
    end
  end
end
