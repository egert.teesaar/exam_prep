Feature: Taxi booking
  As a customer
  I want to place a bet on chosen item
  and get feadback if the bet went through or not

    Scenario: Placing a bet which is higher than previous bets and therefore goes thru
        Given the following items are in the auction
            | name                          | size	    | current_price     |
            | Chicago Bullet Speed Skate    | 7         | 60	            |
            | Riedell Dart Derby Skates     | 8         | 110               |
        And I want to buy item with name "Chicago Bullet Speed Skate" size "7" for price "60" dollars
        And I open Auction' web page
        And I click on item "Chicago Bullet Speed Skate" size "7" on the list
        And I am being navigated to "Chicago Bullet Speed Skate" bet placement page
        And I insert the price "62"
        When I summit the bet placement request
        Then current_price on item "Chicago Bullet Speed Skate" should be updated to "62" dollars

    Scenario: Placing a bet which is lower or equal than previous bets and therefore is rejected
        Given the following items are in the auction
            | name                          | size	    | current_price     |
            | Chicago Bullet Speed Skate    | 7         | 60	            |
            | Riedell Dart Derby Skates     | 8         | 110               |
        And I want to buy item with name "Riedell Dart Derby Skates" size "8" for price "100" dollars
        And I open Auction' web page
        And I click on item "Riedell Dart Derby Skates" size "8" on the list
        And I am being navigated to "Riedell Dart Derby Skates" bet placement page
        And I insert the price "100"
        When I summit the bet placement request
        Then I should receive a rejection message