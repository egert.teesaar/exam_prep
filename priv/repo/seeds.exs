# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Auction.Repo.insert!(%Auction.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will halt execution if something goes wrong.

alias Auction.{Repo, User, Item}

[%{username: "fred", password: "parool"},
 %{username: "barney", password: "parool"}]
|> Enum.map(fn user_data -> User.changeset(%User{}, user_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)

[%{name: "Chicago Bullet Speed Skate", size: "7", price: "60"},
%{name: "Riedell Dart Derby Skates", size: "8", price: "110"}]
|> Enum.map(fn user_data -> Item.changeset(%Item{}, user_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)


