defmodule Auction.ItemTest do
  use Auction.ModelCase

  alias Auction.Item

  @valid_attrs %{name: "some name", price: "some price", size: "some size"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Item.changeset(%Item{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Item.changeset(%Item{}, @invalid_attrs)
    refute changeset.valid?
  end
end
