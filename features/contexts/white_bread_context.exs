defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers
  
  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end
  scenario_starting_state fn _state ->
    Hound.start_session
    %{}
  end
  scenario_finalize fn _status, _state -> 
    nil
  end

  given_ ~r/^the following items are in the auction$/, fn state ->
    {:ok, state}
  end

  and_ ~r/^I want to buy item with name "(?<argument_one>[^"]+)" size "(?<argument_two>[^"]+)" for price "(?<argument_three>[^"]+)" dollars$/,
    fn state, %{argument_one: _argument_one,argument_two: _argument_two,argument_three: _argument_three} ->
      {:ok, state}
  end

  and_ ~r/^I open Auction' web page$/, fn state ->
    navigate_to "/auctions"
    {:ok, state}
  end

  and_ ~r/^I click on item "(?<argument_one>[^"]+)" size "(?<argument_two>[^"]+)" on the list$/,
    fn state, %{argument_one: _argument_one,argument_two: _argument_two} ->
      {:ok, state}
  end

  and_ ~r/^I am being navigated to "(?<argument_one>[^"]+)" bet placement page$/,
    fn state, %{argument_one: _argument_one} ->
      {:ok, state}
  end

  and_ ~r/^I insert the price "(?<argument_one>[^"]+)"$/,
    fn state, %{argument_one: _argument_one} ->
      {:ok, state}
  end

  when_ ~r/^I summit the bet placement request$/, fn state ->
    {:ok, state}
  end

  then_ ~r/^current_price on item "(?<argument_one>[^"]+)" should be updated to "(?<argument_two>[^"]+)" dollars$/,
    fn state, %{argument_one: _argument_one,argument_two: _argument_two} ->
      {:ok, state}
  end

  then_ ~r/^I should receive a rejection message$/, fn state ->
    {:ok, state}
  end

end
